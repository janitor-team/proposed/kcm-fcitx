# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# scootergrisen, 2017,2019
# scootergrisen, 2017,2019-2020
msgid ""
msgstr ""
"Project-Id-Version: fcitx\n"
"Report-Msgid-Bugs-To: fcitx-dev@googlegroups.com\n"
"POT-Creation-Date: 2017-06-09 08:49-0700\n"
"PO-Revision-Date: 2020-04-04 07:42+0000\n"
"Last-Translator: scootergrisen\n"
"Language-Team: Danish (http://www.transifex.com/fcitx/fcitx/language/da/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: da\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: layout/main.cpp:37
#, kde-format
msgid "A general keyboard layout viewer"
msgstr "En generel tastaturlayout-fremviser"

#: layout/main.cpp:41
#, kde-format
msgid "Keyboard layout <group> (0-3)"
msgstr "Tastaturlayout <gruppe> (0-3)"

#: layout/main.cpp:42
#, kde-format
msgid "group"
msgstr "gruppe"

#: layout/main.cpp:44
#, kde-format
msgid "Keyboard <layout>"
msgstr "Tastatur <layout>"

#: layout/main.cpp:45
#, kde-format
msgid "layout"
msgstr "layout"

#: layout/main.cpp:47
#, kde-format
msgid "Keyboard layout <variant>"
msgstr "Tastaturlayout <variant>"

#: layout/main.cpp:48
#, kde-format
msgid "variant"
msgstr "variant"

#: layout/main.cpp:74
#, kde-format
msgid "Keyboard Layout viewer"
msgstr "Tastaturlayout-fremviser"

#: src/addonselector.cpp:375
#, kde-format
msgid "Search Addons"
msgstr "Søg efter tilføjelser"

#: src/addonselector.cpp:382 src/configwidget.cpp:606
#, kde-format
msgid "Show &Advance option"
msgstr "Vis &avanceret valgmulighed"

#: src/configwidget.cpp:450
#, kde-format
msgid "<b>Other</b>"
msgstr "<b>Andre</b>"

#: src/configwidget.cpp:560 src/impage.cpp:69
#, kde-format
msgid "Other"
msgstr "Andre"

#: src/erroroverlay.cpp:43
#, kde-format
msgid "Cannot connect to Fcitx by DBus, is Fcitx running?"
msgstr "Kan ikke oprette forbindelse til Fcitx med DBus - kører Fcitx?"

#: src/imconfigdialog.cpp:42
#, kde-format
msgid "<b>Keyboard Layout:</b>"
msgstr "<b>Tastaturlayout:</b>"

#: src/imconfigdialog.cpp:47
#, kde-format
msgid "Default"
msgstr "Standard"

#: src/imconfigdialog.cpp:49
#, kde-format
msgid "Input Method Default"
msgstr "Standardinputmetode"

#: src/imconfigdialog.cpp:98
#, kde-format
msgid "<b>Input Method Setting:</b>"
msgstr "<b>Inputmetodeindstilling:</b>"

#: src/impage.cpp:50
#, kde-format
msgid "Unknown"
msgstr "Ukendt"

#: src/impage.cpp:53
#, kde-format
msgid "Multilingual"
msgstr "Flersproget"

#: src/impage.cpp:84
#, kde-format
msgctxt "%1 is language name, %2 is country name"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: src/impage.cpp:481
#, kde-format
msgid "Search Input Method"
msgstr "Søg efter inputmetode"

#: src/module.cpp:83
#, kde-format
msgid "Fcitx Configuration Module"
msgstr "Fcitx-konfigurationsmodul"

#: src/module.cpp:85
#, kde-format
msgid "Configure Fcitx"
msgstr "Konfigurer Fcitx"

#: src/module.cpp:87
#, kde-format
msgid "Copyright 2012 Xuetian Weng"
msgstr "Ophavsret 2012 Xuetian Weng"

#: src/module.cpp:91
#, kde-format
msgid "Xuetian Weng"
msgstr "Xuetian Weng"

#: src/module.cpp:91
#, kde-format
msgid "Author"
msgstr "Forfatter"

#: src/module.cpp:104 src/kcm_fcitx.desktop.in:11
#, kde-format
msgid "Input Method"
msgstr "Inputmetode"

#: src/module.cpp:113
#, kde-format
msgid "Global Config"
msgstr "Global konfiguration"

#: src/module.cpp:121
#, kde-format
msgid "Appearance"
msgstr "Udseende"

#: src/module.cpp:129
#, kde-format
msgid "Addon Config"
msgstr "Tilføjelseskonfiguration"

#: src/module.cpp:178
#, kde-format
msgid "Manage Skin"
msgstr "Håndter skin"

#: src/skinpage.cpp:220
#, kde-format
msgid "First candidate"
msgstr "Første kandidat"

#: src/skinpage.cpp:221
#, kde-format
msgid "Other candidate"
msgstr "Anden kandidat"

#: src/skinpage.cpp:390
#, kde-format
msgid "Skin %1 Cannot be loaded"
msgstr "Skin %1 kan ikke indlæses"

#: src/subconfigwidget.cpp:230
#, kde-format
msgid ""
"User config doesn't exisits, do you want to open system file or copy system "
"file to user file?"
msgstr "Brugerkonfigurationen findes ikke - vil du åbne systemfilen eller kopiere systemfilen til brugerfilen?"

#: src/subconfigwidget.cpp:231
#, kde-format
msgid "What to do"
msgstr "Skal gøres"

#: src/subconfigwidget.cpp:232
#, kde-format
msgid "Copy"
msgstr "Kopiér"

#: src/subconfigwidget.cpp:233
#, kde-format
msgid "View system"
msgstr "Vis system"

#: src/subconfigwidget.cpp:245
#, kde-format
msgid "Copy failed"
msgstr "Kopiering mislykkedes"

#: src/uipage.cpp:16
#, kde-format
msgid "Cannot load currently used user interface info"
msgstr "Kan ikke indlæse information om brugergrænseflade som bruges i øjeblikket"

#: src/uipage.cpp:45
#, kde-format
msgid "No configuration options for %1."
msgstr "Ingen konfigurationsvalgmuligheder for %1."

#: layout/kbd-layout-viewer.desktop.in:6
msgid "Keyboard layout viewer"
msgstr "Tastaturlayout-fremviser"

#: layout/kbd-layout-viewer.desktop.in:7
msgid "View keyboard layout"
msgstr "Vis tastaturlayout"

#: layout/kbd-layout-viewer.desktop.in:9
msgid "keyboard;input;im"
msgstr "tastatur;input;im"

#: src/kcm_fcitx.desktop.in:12
msgid "Configure Input Method"
msgstr "Konfigurer inputmetode"

#: src/kcm_fcitx.desktop.in:14
msgid "keyboard,input,im,fcitx"
msgstr "tastatur;input;im;fcitx"

#. i18n: file: src/configpage.ui:14
#. i18n: ectx: property (windowTitle), widget (QWidget, ConfigPage)
#. i18n: file: src/fontbutton.ui:20
#. i18n: ectx: property (windowTitle), widget (QWidget, FontButton)
msgid "Form"
msgstr "Formular"

#. i18n: file: src/configpage.ui:54
#. i18n: ectx: property (text), widget (QLabel, infoLabel)
msgid ""
"Options listed here might be overridden by specific input method, usually "
"including candidate number, hotkey of previous page and next page. You may "
"need go to configuration of corresponding input method to change them."
msgstr "Valgmuligheder som vises her kan tilsidesættes af specifik inputmetode, oftest med kandidatnummer, hottast af forrige side og næste side. Det kan være at du skal gå til konfiguration af tilhørende inputmetode for at ændre dem."

#. i18n: file: src/fontbutton.ui:51
#. i18n: ectx: property (text), widget (QPushButton, fontSelectButton)
msgid "Select &Font..."
msgstr "Vælg &skrifttype ..."

#. i18n: file: src/impage.ui:38
#. i18n: ectx: property (toolTip), widget (QPushButton, defaultLayoutButton)
msgid "Keyboard layout to use when no input method active"
msgstr "Tastaturlayout som skal bruges når der ikke er nogen aktiv inputmetode"

#. i18n: file: src/impage.ui:41
#. i18n: ectx: property (text), widget (QPushButton, defaultLayoutButton)
msgid "Select default keyboard layout"
msgstr "Vælg standardtastaturlayout"

#. i18n: file: src/impage.ui:83
#. i18n: ectx: property (text), widget (QLabel, label)
msgid "Available Input Method:"
msgstr "Tilgængelig inputmetode:"

#. i18n: file: src/impage.ui:115
#. i18n: ectx: property (text), widget (QCheckBox,
#. onlyCurrentLanguageCheckBox)
msgid "Only &Show Current Language"
msgstr "&Vis kun nuværende sprog"

#. i18n: file: src/impage.ui:176
#. i18n: ectx: property (text), widget (QLabel, label_2)
msgid "Current Input Method:"
msgstr "Nuværende inputmetode:"

#. i18n: file: src/impage.ui:277
#. i18n: ectx: property (text), widget (QLabel, infoLabel)
msgid ""
"The first input method will be inactive state. Usually you need to put "
"<b>Keyboard</b> or <b>Keyboard - <i>layout name</i></b> in the first place."
msgstr "Den første inputmetode vil være i inaktiv tilstand. Typisk skal der først stå <b>Tastatur</b> eller <b>Tastatur - <i>layoutnavn</i></b>."

#. i18n: file: src/impage.ui:292
#. i18n: ectx: property (text), widget (QLabel, label_3)
msgid "<b>Default keyboard layout:</b>"
msgstr "<b>Standardtastaturlayout:</b>"

#. i18n: file: src/impage.ui:302
#. i18n: ectx: property (text), widget (QLabel, label_4)
msgid "<b>Select Input Method:</b>"
msgstr "<b>Vælg inputmetode:</b>"

#. i18n: file: src/skinpage.ui:41
#. i18n: ectx: property (text), widget (QPushButton, configureSkinButton)
msgid "&Configure Skin"
msgstr "&Konfigurer skin"

#. i18n: file: src/skinpage.ui:51
#. i18n: ectx: property (text), widget (QPushButton, deleteSkinButton)
msgid "&Delete Skin"
msgstr "&Slet skin"

#. i18n: file: src/skinpage.ui:58
#. i18n: ectx: property (text), widget (QPushButton, installSkinButton)
msgid "Get New &Skin..."
msgstr "Hent nyt &skin ..."

#. i18n: file: src/skinpage.ui:72
#. i18n: ectx: property (text), widget (QLabel, label)
msgid ""
"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Notice:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">You need to use Classic UI to use skin function.</p></body></html>"
msgstr "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Bemærk:</span></p>\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Du skal bruge klassisk brugerflade for at bruge skin-funktion.</p></body></html>"

msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Dine navne"

msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "Dine e-mails"
